import Vue from 'vue';
import * as moment from "moment";
import 'moment/locale/ru';

const Moment = class VueMoment {
	/**
	 * @description
	 * moment.js function
	 * @type {*}
	 */
	static moment = moment;
	static install(Vue) {
		this.moment.locale('ru');
		Vue.prototype.$moment = this.moment;
	}
};

Vue.use(Moment);

export default Moment;