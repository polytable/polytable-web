import Vue from 'vue'
import App from './App.vue'
import moment from './plugins/moment';
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;

new Vue({
	moment,
	vuetify,
	render: h => h(App)
}).$mount('#app');
